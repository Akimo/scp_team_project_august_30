#pragma once
#include<windows.h>

#define ScreenWidth 80
#define ScreenHeight 20
class CWINDOW
{
	HANDLE hOut; //前台窗口 
	static HANDLE buffer; //后台窗口

	COORD size = { ScreenWidth,ScreenHeight };
	CHAR_INFO  *chiBuffer;
	COORD buffersize;
	COORD buffercoord;
	SMALL_RECT srctReadRectforbuffer;
	SMALL_RECT srctWriteRectforbuffer;
public:
	void newfrontwindow(); //创建前台窗口 
	void newbackwindow(); //创建后台窗口
	void clear();    //清屏
	void flush();  //双缓冲
	static bool keyborad(int key); //按键 

	static void draw_TWOdim(short x, short y, char ** pic, short wid, short hei);
	static void draw_ONEdim(short x, short y, char * pic);
	void draw2(int base);
	CWINDOW();
	~CWINDOW();
};

