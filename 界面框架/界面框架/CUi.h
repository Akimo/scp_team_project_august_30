#pragma once
class CUser;
/*创建一个用户管理系统
界面---开始界面
   1、进入
   2、退出
界面---管理界面
   1、查看所有用户--->进入查看所有用户界面
   2、查看指定用户-->进入查看指定用户界面
   3、插入用户-->进入插入用户界面
   4、删除用户-->进入删除用户界面
   5、退出
查看所有用户界面
   1、退出
查看指定用户界面
   1、用户名
插入用户界面
   1、用户名
   2、用户职业
   3、用户手机号
   4、退出
删除用户界面
   1、用户名
   2、退出
*/
class CUi
{
	CUser**Alluser;
	int Totaluser;
	bool _adduser(const char *na, const char *jo, unsigned long&ph, bool &se, unsigned short &age);
	void _deleteuser(short number);
	void _checkAlluser();
	void _checkUser(const char *&);

public:
	CUi(int num=20);
	void Ui_begin(void);
	bool Ui_adduser(bool);
	bool Ui_deleteuser(bool);
	void Ui_checkUser(const char *&);
	void Ui_checkAlluser(void);
	void PresetUser(const char *filename);
	~CUi();
};

