#pragma once
/*创建一个用户管理系统
界面---开始界面
   1、进入
   2、退出
界面---管理界面
   1、查看所有用户--->进入查看所有用户界面
   2、查看指定用户-->进入查看指定用户界面
   3、插入用户-->进入插入用户界面
   4、删除用户-->进入删除用户界面
   5、退出
查看所有用户界面
   1、退出
查看指定用户界面
   1、用户名
插入用户界面
   1、用户名
   2、用户职业
   3、用户手机号
   4、退出
删除用户界面
   1、用户名
   2、退出
*/
class CUser
{
	unsigned long Phonenumber;
	char User_Name[20];
	char Job[20];
	bool Sex;
	unsigned short Age;
	friend class CUi;
	static int number;
public:
	CUser(unsigned long &ph, const char *na, const char *job, bool &se, unsigned short &age);
	~CUser();
};

