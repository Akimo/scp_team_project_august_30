#include "CWINDOW.h"
#include<windows.h>

HANDLE CWINDOW::buffer = NULL;


void CWINDOW::newfrontwindow()
{

	hOut = GetStdHandle(STD_OUTPUT_HANDLE); //窗口句柄
	CONSOLE_SCREEN_BUFFER_INFO bInfo; // 窗口缓冲区信息
	GetConsoleScreenBufferInfo(hOut, &bInfo);// 获取窗口缓冲区信息

	SetConsoleTitle("带枪小智");

	SetConsoleScreenBufferSize(hOut, size);
	//cin.get();
	SMALL_RECT rc = { 0,0,size.X - 1,size.Y - 1 };
	SetConsoleWindowInfo(hOut, true, &rc);

	DWORD tian = ScreenWidth * ScreenHeight;

	//FillConsoleOutputAttribute(hOut, BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN,tian,{0,0},&pt);
	SetConsoleTextAttribute(hOut, BACKGROUND_BLUE|BACKGROUND_GREEN|BACKGROUND_RED|FOREGROUND_GREEN|FOREGROUND_RED);
}

void CWINDOW::newbackwindow()
{
	DWORD pt;
	buffer = CreateConsoleScreenBuffer(
		GENERIC_READ | GENERIC_WRITE, // 访问权限
		FILE_SHARE_READ | FILE_SHARE_WRITE, // 共享控制
		NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	if (INVALID_HANDLE_VALUE == buffer)
	{
		WriteConsoleOutputCharacterA(hOut, "创建屏幕缓冲失败，错误码", 25, { 0,0 }, &pt);
	}

	//SetConsoleScreenBufferSize(bufferwatch, );

	//存放像素大小
	chiBuffer = new CHAR_INFO[size.Y*size.X];

	buffersize = size; buffersize.X = size.X;   //设置后台画布大小的坐标
												//设置后台画布的大小
	SetConsoleScreenBufferSize(buffer, buffersize);
	buffercoord = { 0,0 };                          //设置后台画布读取写入的起始坐标

	srctReadRectforbuffer = { 0,0,buffersize.X,buffersize.Y };            //设置读取坐标4顶点
	srctWriteRectforbuffer = { 0,0,buffersize.X,buffersize.Y };           //这是写入坐标4顶点
}

void CWINDOW::clear()
{
	DWORD pt;
	for (short a = 0; a < ScreenHeight; a++)
	{
		FillConsoleOutputCharacter(buffer, ' ', ScreenWidth , { 0,a }, &pt);
	}
}

void CWINDOW::flush()
{
	if (chiBuffer == NULL)
		return;


	//读取后台画布上的所有象素
	ReadConsoleOutput(
		buffer,		// screen to read from 
		chiBuffer,      // buffer to copy into 
		buffersize,   // col-row size of chiBuffer 
		buffercoord,  // top left dest. cell in chiBuffer 
		&srctReadRectforbuffer);



	//把这些象素拷贝到前台画布
	WriteConsoleOutput(
		hOut,  // screen buffer to write to 
		chiBuffer,        // buffer to copy from 
		buffersize,     // col-row size of chiBuffer 
		buffercoord,    // top left src cell in chiBuffer 
		&srctWriteRectforbuffer);
}

bool CWINDOW::keyborad(int a)
{
	if (GetAsyncKeyState(a))
	{
		return true;
	}

	return false;
}

void CWINDOW::draw_TWOdim(short x, short y, char ** pic, short wid, short hei)
{
	DWORD pt;
	for (short a = y+1,b=0; a < hei+y+1; a++,b++)
	{
		WriteConsoleOutputCharacterA(buffer, pic[b], wid, { x,a }, &pt);
	}
}
void CWINDOW::draw_ONEdim(short x, short y, char * pic)
{
	DWORD pt;
	
		WriteConsoleOutputCharacterA(buffer, pic,strlen(pic), { x,y }, &pt);
	
}
void CWINDOW::draw2(int base)
{
	DWORD pt;
	int k = ScreenHeight;
	for (short i = 0; i < ScreenWidth; i++)
	{


		if (i%base!=0) { continue; }
		for (short j = 0; j < ScreenHeight; j++) {
			FillConsoleOutputCharacter(buffer,' ', 1, { i,j }, &pt);
		}
	}
}

CWINDOW::CWINDOW()
{
	newfrontwindow();
	newbackwindow();
}


CWINDOW::~CWINDOW()
{
	delete[]  chiBuffer;
}
